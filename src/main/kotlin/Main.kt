import java.io.File
/**
 * JUDGE
 * Es una aplicación con una serie de retos. La aplicación proporciona al jugador un input privado y este debe responder con
 * el output correcto. El juego no termina hasta que el jugador complete todos los retos o decida irse sin resolverlos.
 * @author Paula Garcia Dopico */

/**
 * Este método deja un registro del historial en el fichero "historial.txt".
 * @param user El nombre del jugador.
 * @param falladas El número de preguntas falladas.
 * @param acertadas El número de preguntas acertadas.*/
fun registrarHistorial(user: String, falladas: Int, acertadas: Int) {
    val pathHistorial = "src/files/historial.txt"
    if (File(pathHistorial).length() == 0L) { //Control de contingut a userdata.txt (si té o no contingut)
        File(pathHistorial).appendText("$user, $acertadas, $falladas")
    } else {
        File(pathHistorial).appendText("\n$user, $acertadas, $falladas")
    }
}
/**
 * Este método permite ver todos los registros que hay en el fichero "historial.txt.
 * De momento no está implementado en el juego. Si no se llega a implementarse será eliminado. */
fun mostrarFicheroHistorial() {
    val pathHistorial = "src/files/historial.txt"
    val historial = File(pathHistorial).readText().split("\n").toMutableList()
    for (i in historial.indices){
        val usersR = historial[i].split(",").toTypedArray()
        println("Nombre: ${usersR[0]} \n" +
                "Acertadas: ${usersR[1]}\n" +
                "Falladas: ${usersR[2]}\n" +
                "~~~~~~~~~~~~~~~~~~~~~~~~")
    }
}
/**
 * Este método muestra al usuario las estadísticas al terminar una ronda de preguntas.
 * @param user El nombre del jugador.
 * @param falladas El número de preguntas falladas.
 * @param acertadas El número de preguntas acertadas.
 * @param preguntas Es una mutableList en el qual se encuentra una copia de los objetos "preguntas". */
fun mostrarHistorial(user: String, falladas: Int, acertadas: Int,preguntas: MutableList<Preguntas>) {
    println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" +
            "Nombre: ${user} \n" +
            "Acertadas: ${falladas}\n" +
            "Falladas: ${acertadas}\n")
    for (i in preguntas.indices) {
        if (preguntas[i].resuelto) println("¡Pregunta superada!")
        else println("Pregunta NO superada")
        println("Pregunta: ${ preguntas[i].id } \n" +
                "Se ha realizado con: ${preguntas[i].intentos} intentos\n" +
                "La lista de intentos usados: ${preguntas[i].listaIntentos}")
    }
    println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" )
}
/**
 * Esta función comprueba si han sido todas als preguntas resueltas. Se utilitza para verificar si el juego puede finalizar.
 * @param preguntas Es una mutableList en el qual se encuentra una copia de los objetos "preguntas".
 * @return Boolean: devuelve false, si alguna pregunta no ha sido resuelta. */
fun verificarResueltas(preguntas: MutableList<Preguntas>):Boolean {
    for (i in preguntas.indices) {
        if (!preguntas[i].resuelto) {
            return false
        }
    }
    return true
}
fun main() {
    var input: String
    val pregunta1 = Preguntas(
        1,"Haz un programa que reciba una temperatura en grados Celsius y la convierta en grados Fahrenheit",
        "33.1", "91.58", "-3.2", "26.2",
    )
    val pregunta2 = Preguntas(
        2,"Haz un programa que calcule el equivalente en pie una longitud en metros.\n" +
                "Ten en cuenta que:\n" +
                "   -1 metro = 39,37 pulgadas\n" +
                "   -12 pulgadas = 1 pie",
        "30", "98.4251969", "515", "1689.63255",
    )
    val pregunta3 = Preguntas(
        3,"Lee un valor con decimales e imprime el doble.",
        "2.1", "4.2", "123.456", "246.912",
    )
    val pregunta4 = Preguntas(
        4,"Lee el precio original y el precio actual e imprime el descuento (en %).",
        "1000 800", "20", "540 420", "22.22",)
    val pregunta5 = Preguntas(
        5,"Lee el diámetro de una pizza redonda e imprime su superficie. Puedes utilizar Math.PI para escribir el valor de Pi.",
        "38", "1133.54", "55", "2375.83",)

    val ejercicios = mutableListOf<Preguntas>(pregunta1,pregunta2,pregunta3,pregunta4,pregunta5) //Para que sea más fácil de manipular y no se repita código.
    var id = 0
    var falladas: Int = 0
    var acertadas: Int = 0
    var stopJudge: Boolean = false
    do {
        do {
            ejercicios[id].mostrarIntentos()
            var resuelto: Boolean
            if (!ejercicios[id].resuelto) {
                do {
                    println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                    ejercicios[id].mostrarEnunciado()
                    ejercicios[id].mostrarJuegoPublico()
                    ejercicios[id].retoJuegoPrivado()
                    input = readln()
                    ejercicios[id].listaIntentos.add(input) //Solo se muestra si ha realizado mas de un intento.
                    ejercicios[id].intentos++
                    resuelto = ejercicios[id].correccionPregunta(input)
                    if (resuelto) {
                        acertadas++
                        ejercicios[id].resuelto = true
                    } else {
                        if (!resuelto) {
                            do {
                                println("¿Deseas volver a intentarlo? Escribe 'si' o 'no'.")
                                input = readln()
                                if (input == "no") {
                                    falladas++
                                    resuelto = true
                                }
                            } while (input != "no" && input != "si")
                        }
                    }
                } while (!resuelto)
            }
            id++
        } while (id != 5)
        println("Se han acabado todas las preguntas, por favor registra tu usuario: ")
        input = readln()
        mostrarHistorial(input,acertadas,falladas,ejercicios)
        registrarHistorial(input,acertadas,falladas)
        if (verificarResueltas(ejercicios)) stopJudge = true //Verifica si todas las preguntas han sido completadas, en caso de ser true finaliza el juego (ya que no quedan más preguntas.)
        else {
            do {
                println("¿Deseas volver a intentarlo? Escribe 'si' o 'no'.")
                input = readln()
                if (input == "no") stopJudge = true
                id = 0
            } while (input != "si" && input != "no")
        }
    } while (!stopJudge)
}