class Preguntas(val id : Int, val enunciado: String, val inputPublico: String,val outputPublico: String,val inputPrivado: String,val outputPrivado:String) {
    var resuelto: Boolean = false
    var intentos: Int = 0
    var listaIntentos = mutableListOf<String>()
    /** Muestra el enunciado al jugador. */
    fun mostrarEnunciado() {
        println(enunciado)
    }
    /** Muestra el juego de pruebas público. A diferencia del privado este si proporciona la respuesta. */
    fun mostrarJuegoPublico() {
        println("Juego de pruebas\n" +
                "INPUT\n" +
                "$inputPublico\n" +
                "OUTPUT\n" +
                "$outputPublico")
    }
    /** Muestra el juego de pruebas privado. El jugador a continuación debe entregar su respuesta.*/
    fun retoJuegoPrivado() {
        println("Ponte a prueba \n" +
                "Inserta el siguiente input en tu programa y registra el resultado obtenido: \n" +
                "INPUT\n" +
                "$inputPrivado\n" +
                "OUTPUT")
    }
    /**
     * Compara la respuesta proporcionada con el jugador con la respuesta que hay registrada en el objeto.
     * Si esta es correcta devuelve true.
     * @param inputJugador Contiene la respuesta proporcionada por el jugador.
     * @return Boolean: Si la respuesta es correcta devuelve true. */
    fun correccionPregunta(inputJugador: String):Boolean {
        if (inputJugador == outputPrivado) {
            println("¡Felicidades la respuesta es correcta!")
            return true
        } else {
            println("La respuesta no es correcta")
            return false
        }
    }
    /** Si la pregunta ha sido intentada más de una vez muestra el número de intentos y las respuestas que ya
     * ha intentado anteriormente. */
    fun mostrarIntentos() {
        if (intentos > 0) {
            if (resuelto) println("¡Pregunta superada!")
            println("Pregunta $id \n" +
                    "Has intentado esta pregunta: $intentos \n" +
                    "Tus intentos han sido $listaIntentos \n")
        }
    }
}