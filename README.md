# JUDGE

Judge es una aplicación que propone retos a los jugadores. Cuando empieza una partida lanza un enunciado, este proporciona un juego de pruebas público: 
- INPUT: La entrada con la que realizar pruebas. 
- OUTPUT: La salida que devuelve al introducir el input.

Para finalizar da al jugador un juego de pruebas privado, a diferencia del público este no muestra la respuesta. Cuando el jugador ingresa su respuesta en la aplicación esta comprueba si esta es correcta y se lo comunica al usuario. 

## Como se juega

Hay un total de cinco preguntas. Cuando inicias la aplicación esta te muestra el primer enunciado, junto al juego de pruebas público. Proporciona la entrada del juego de pruebas privado y espera la respuesta del jugador.

````
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Haz un programa que reciba una temperatura en grados Celsius y la convierta en grados Fahrenheit
Juego de pruebas
INPUT
33.1
OUTPUT
91.58
Ponte a prueba 
Inserta el siguiente input en tu programa y registra el resultado obtenido: 
INPUT
-3.2
OUTPUT
````

Si la salida del jugador es correcta pasará a la siguiente pregunta. Cuando falla una pregunta se le da la posibilidad de repetirla, en caso de responder "no" pasara a la siguiente.

Al finalizarlas registrará el usuario del jugador y mostrará unas estadísticas que seran guardadas en un fichero. Luego permitirá al jugador repetir aquellas preguntas que no han sido respondidas correctamente. 

````
Se han acabado todas las preguntas, por favor registra tu usuario: 
Paula
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nombre: Paula 
Acertadas: 3
Falladas: 2

¡Pregunta superada!
Pregunta: 1 
Se ha realizado con: 2 intentos
La lista de intentos usados: [12.0, 26.2]
¡Pregunta superada!
Pregunta: 2 
Se ha realizado con: 1 intentos
La lista de intentos usados: [1689.63255]
¡Pregunta superada!
Pregunta: 3 
Se ha realizado con: 1 intentos
La lista de intentos usados: [246.912]
Pregunta NO superada
Pregunta: 4 
Se ha realizado con: 2 intentos
La lista de intentos usados: [12.0, 20.0]
Pregunta NO superada
Pregunta: 5 
Se ha realizado con: 1 intentos
La lista de intentos usados: [123654.54]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

¿Deseas volver a intentarlo? Escribe 'si' o 'no'.
````
En la siguiente ronda de preguntas tras volver a intentarlo, mostrará los intentos de las preguntas resueltas anteriormente y los intentos ya realizados en las no completadas.

````
¡Pregunta superada!
Pregunta 1 
Has intentado esta pregunta: 2 
Tus intentos han sido [12.0, 26.2] 

¡Pregunta superada!
Pregunta 2 
Has intentado esta pregunta: 1 
Tus intentos han sido [1689.63255] 

¡Pregunta superada!
Pregunta 3 
Has intentado esta pregunta: 1 
Tus intentos han sido [246.912] 

Pregunta 4 
Has intentado esta pregunta: 2 
Tus intentos han sido [12.0, 20.0] 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Lee el precio original y el precio actual e imprime el descuento (en %).
Juego de pruebas
INPUT
1000 800
OUTPUT
20
Ponte a prueba 
Inserta el siguiente input en tu programa y registra el resultado obtenido: 
INPUT
540 420
OUTPUT
````

De esta manera sigue hasta que el jugador decide no continuar o completa todos los retos.
## Actualizaciones

[22/02/2023] - Creación del juego y documentación en Dokka. A la espera de nuevas actualizaciones.

## Errores que contienen

De momento no se contempla ningun error. 

## Pruebas unitarias realizadas

No hay test unitarios realizados, tampoco se han requerido.

## Creado con

* [Kotlin]() - El lenguaje de programación.
* [IntellJ]() - El IDE.


## Autor

* **Paula García Dopico**